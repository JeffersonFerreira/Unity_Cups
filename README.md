# Unity Cups

This is a demo project where the challenge was to improve both the code, side and performance.

Unfortunately I only have one physical device to test, and because it is a high performance device (galaxy S10e), it is a little difficult to see the results.

___

Anyway, I worked hard to improve the organization and separation of the code and hierarchy the best i could.
There is still room for more improvement, but the deadline has arrived.

The use of GPU and RAM ended up being similar, but I had a good improvement in the CPU.

Using the profiller, it is possible to see that 25% of the CPU is being used, allocating 82 Bytes per frame (2.46 kB per second)

After my refactoring, the CPU usage dropped to 20% while the memory allocation was close to 0 bytes per frame.
Every second I need to allocate a little using `string.Format()`


X       | Before        | After
---     | ----          | ----
CPU     | 25%           | 20%
CPU     | 18 DrawCalls  | 18 DrawCalls 
RAM     | 9.8 MB        | 9.4 MB
Size    | ?             | 21.3 MB


[Apk download link](https://drive.google.com/file/d/1bLw4kLToXvjjRLIAnv1WZFSklVwctb5n/view?usp=sharing)