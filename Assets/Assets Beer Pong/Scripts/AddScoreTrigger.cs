﻿using UnityEngine;
using UnityEngine.Serialization;

public class AddScoreTrigger : MonoBehaviour
{
	[FormerlySerializedAs("Total_Score")]
	[SerializeField] private int _incrementAmount;

	[Space]
	[FormerlySerializedAs("Point_Iniciate")]
	[SerializeField] private Transform _popupSpawnPoint;

	[Header("Prefabs")]
	[SerializeField][FormerlySerializedAs("Particles")] private GameObject _particlePrefabs;
	[SerializeField][FormerlySerializedAs("Points")]  private GameObject _scorePopupPrefab;
	[SerializeField][FormerlySerializedAs("Bonus")]  private GameObject _bonusPopupPrefab;

	private Transform _camTransform;
	private AudioSource _audioSource;

	private GameManager _gameManager;

	private void Awake()
	{
		_gameManager = FindObjectOfType<GameManager>();
		_camTransform = Camera.main.transform;
		_audioSource = GetComponent<AudioSource>();
	}

	private void OnTriggerEnter(Collider other)
	{
		GameObject ball = other.gameObject;

		if (other.CompareTag("Beer_pong"))
		{
			Score();
			Destroy(ball);
		}

		if (other.CompareTag("Doble_Pong"))
		{
			ScoreBonus();
			Destroy(ball);
		}
	}

	private void Score()
	{
		Vector3 pos = _popupSpawnPoint.position;
		Quaternion rot = _camTransform.rotation;

		Instantiate(_scorePopupPrefab, pos, rot);
		Instantiate(_particlePrefabs, pos, rot);

		_audioSource.Play();
		// _gameManager.score += _incrementAmount;
		_gameManager.AddScore(_incrementAmount);
	}

	private void ScoreBonus()
	{
		Vector3 pos = _popupSpawnPoint.position;
		Quaternion rot = _camTransform.rotation;

		Instantiate(_bonusPopupPrefab, pos, rot);
		Instantiate(_particlePrefabs, pos, rot);

		_audioSource.Play();
		// _gameManager.score += _incrementAmount * 2;
		_gameManager.AddScore(_incrementAmount * 2);
	}
}