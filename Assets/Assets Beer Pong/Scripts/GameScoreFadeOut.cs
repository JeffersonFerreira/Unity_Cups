﻿using DG.Tweening;
using UnityEngine;

namespace BeerPong
{
	public class GameScoreFadeOut : MonoBehaviour
	{
		[SerializeField] private float _waitUntilFadeStarts = 0.5f;
		[SerializeField] private float _fadeDuration = 0.5f;

		private SpriteRenderer _spriteRenderer;

		private void Awake()
		{
			_spriteRenderer = GetComponent<SpriteRenderer>();
		}

		private void Start()
		{
			Destroy(gameObject, 3.0f);

			_spriteRenderer.color = Color.white;
			_spriteRenderer.DOFade(0, _fadeDuration).SetDelay(_waitUntilFadeStarts);
		}
	}
}