using UnityEngine;
using UnityEngine.UI;

namespace BeerPong
{
	public class BallShooterHud : MonoBehaviour
	{
		[SerializeField] private Slider _forceSlider;
		[SerializeField] private Transform _aimTransform;

		private BallShooter _shooter;

		private void Awake()
		{
			_shooter = FindObjectOfType<BallShooter>();

			_shooter.OnShoot += Shooter_OnShoot;
			_shooter.OnShootCooldown += Shooter_OnShootCooldown;
		}

		private void Shooter_OnShoot(GameObject ball, GameObject shadow)
		{
			_forceSlider.value = 0;
			_forceSlider.gameObject.SetActive(false);

			Vector3 eulerAngles = _aimTransform.eulerAngles;
			// No idea why this value, just copy and pasted form transform inspector
			eulerAngles.y = 358.8f;
			_aimTransform.eulerAngles = eulerAngles;
		}

		private void Shooter_OnShootCooldown()
		{
			_forceSlider.gameObject.SetActive(true);
		}
	}
}