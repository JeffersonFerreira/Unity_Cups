using UnityEngine;
using UnityEngine.UI;

namespace BeerPong.UI
{
	public class QuitGameView : ViewBase
	{
		[SerializeField] private Button _noButton;
		[SerializeField] private Button _yesButton;

		private GameManager _gameManager;

		private void Awake()
		{
			_gameManager = FindObjectOfType<GameManager>();
		}

		private void Start()
		{
			_yesButton.onClick.AddListener(_gameManager.Exit);
			_noButton.onClick.AddListener(_gameManager.CancelPause);
		}
	}
}