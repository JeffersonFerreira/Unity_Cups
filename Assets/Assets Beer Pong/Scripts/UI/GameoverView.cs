using UnityEngine;
using UnityEngine.UI;

namespace BeerPong.UI
{
	public class GameoverView : ViewBase
	{
		[SerializeField] private Text _totalScoreText;

		[Space]
		[SerializeField] private Button _exitButton;
		[SerializeField] private Button _tryAgainButton;

		private GameManager _gameManager;

		private void Awake()
		{
			_gameManager = FindObjectOfType<GameManager>();
			_gameManager.OnGameOver += OnGameOver;
		}

		private void Start()
		{
			_exitButton.onClick.AddListener(_gameManager.Exit);
			_tryAgainButton.onClick.AddListener(_gameManager.TryAgain);
		}

		private void OnGameOver()
		{
			_totalScoreText.text = _gameManager.score.ToString();
		}
	}
}