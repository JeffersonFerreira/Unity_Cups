using UnityEngine;

namespace BeerPong.UI
{
	public abstract class ViewBase : MonoBehaviour
	{
		public virtual void Show() => gameObject.SetActive(true);

		public virtual void Hide() => gameObject.SetActive(false);
	}
}