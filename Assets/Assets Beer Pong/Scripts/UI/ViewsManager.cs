using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BeerPong.UI
{
	public class ViewsManager : MonoBehaviour
	{
		private Dictionary<Type, ViewBase> _views;

		private void Awake()
		{
			var gameManager = FindObjectOfType<GameManager>();
			_views = GetComponentsInChildren<ViewBase>(true).ToDictionary(view => view.GetType());

			gameManager.OnGameOver += GameManager_OnGameOver;
		}

		private void GameManager_OnGameOver()
		{
			GetView<GameoverView>().Show();
		}

		public T GetView<T>() where T : ViewBase
		{
			Type key = typeof(T);
			return _views[key] as T;
		}
	}
}