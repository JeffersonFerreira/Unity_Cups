using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace BeerPong.UI
{
	public class GameplayView : ViewBase
	{
		[SerializeField] private Text _timerText;
		[SerializeField] private Text _scoreText;

		[Space]
		[SerializeField] private Button _backButton;

		private GameManager _gameManager;

		private void Awake()
		{
			_gameManager = FindObjectOfType<GameManager>();
			_gameManager.OnScoreChanged += OnScoreChanged;
		}

		private void Start()
		{
			_backButton.gameObject.SetActive(!_gameManager.isTournament);
			_backButton.onClick.AddListener(_gameManager.Pause);

			StartCoroutine(UpdateTimer());
		}

		IEnumerator UpdateTimer()
		{
			var wait = new WaitForSeconds(1f);
			const string format = @"mm\:ss";

			while (true)
			{
				yield return wait;
				_timerText.text = TimeSpan.FromSeconds(_gameManager.timer).ToString(format);
			}
		}

		private void OnScoreChanged(int newScore)
		{
			_scoreText.text = newScore.ToString();
		}
	}
}