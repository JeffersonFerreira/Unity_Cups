﻿using DG.Tweening;
using UnityEngine;

namespace BeerPong
{
	[RequireComponent(typeof(AudioSource))]
	public class BallSound : MonoBehaviour
	{
		[SerializeField] private int _triggerMinVelocity = 2;

		[Space]
		[SerializeField] private float _volumeIncrementPerHit = -1;
		[SerializeField] private float _pitchIncrementPerHit = 0.1f;

		[Range(0, 3)]
		[SerializeField] private float _maxPitch = 2.8f;

		private float _pitchAccumulator;
		private float _volumeAccumulator;

		private AudioSource _audioSource;

		private void Awake()
		{
			_audioSource = GetComponent<AudioSource>();
		}

		private void OnCollisionEnter(Collision collision)
		{
			if (collision.relativeVelocity.magnitude <= _triggerMinVelocity)
				return;

			_audioSource.Play();

			// Update Accumulators
			_pitchAccumulator += _pitchIncrementPerHit;
			_volumeAccumulator += _volumeIncrementPerHit;

			// Apply values to source with transition to avoid the sudden change feeling
			DOTween.Sequence()
				.Append(_audioSource.DOFade(_audioSource.volume + _volumeAccumulator, 1))
				.Join(_audioSource.DOPitch(Mathf.Min(_maxPitch, _audioSource.pitch + _pitchAccumulator), 1))
				.SetLink(gameObject);
		}
	}
}