using System.Collections.Generic;
using UnityEngine;

namespace BeerPong
{
	public class RemoveExtraBalls : MonoBehaviour
	{
		[SerializeField] private int _maxBallsCount = 3;

		private BallShooter _shooter;

		private readonly Queue<QueueData> _queue = new Queue<QueueData>();

		private void Awake()
		{
			_shooter = FindObjectOfType<BallShooter>();
			_shooter.OnShoot += OnBallShot;
		}

		private void OnBallShot(GameObject ball, GameObject shadow)
		{
			_queue.Enqueue(new QueueData(ball, shadow));

			int queueCount = _queue.Count;

			while (queueCount > _maxBallsCount)
			{
				QueueData data = _queue.Dequeue();

				if (!data.IsNull())
				{
					queueCount--;
					data.Destroy();
				}
			}
		}

		public readonly struct QueueData
		{
			public readonly GameObject ball;
			public readonly GameObject shadow;

			public QueueData(GameObject ball, GameObject shadow)
			{
				this.ball = ball;
				this.shadow = shadow;
			}

			public bool IsNull()
			{
				return ball == null;
			}

			public void Destroy()
			{
				Object.Destroy(ball);
				Object.Destroy(shadow);
			}
		}
	}
}