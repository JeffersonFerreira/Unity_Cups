﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[SerializeField] private int _timerInitialValue = 60;

	public int Time_update;
	public int Time_update_Max;

	[Space]
	public GameObject PopUpExit;
	public GameObject HUD;

	[Space]
	public Transform BarForce;
	public Transform Bar_indForce;

	[Space]
	public GameObject Ball_img;

	[Space]
	public GameObject Validate_Force;
	public GameObject Validate_Force2;

	public event Action<int> OnScoreChanged;
	public event Action OnGameOver;

	public int score { get; private set; }
	public int timer { get; private set; }

	public bool isTournament { get; private set; }
	public bool isEndlessTime { get; private set; }

	private void Awake()
	{
		// Check if is practice mode
		// tournamentID == 1 is practice
		isTournament = PlayerPrefs.GetString("tournamentID") != "1";
		isEndlessTime = PlayerPrefs.GetInt("game_mode") == 1;
	}

	private void Start()
	{
		// I have no idea what the purpose of these things is
		Time.fixedDeltaTime = 0.02F * Time.timeScale;
		Time.timeScale = 1f;
		Time_update_Max = int.Parse(PlayerPrefs.GetString("updateTime"));
		Time_update = Time_update_Max;
		StartCoroutine("UpdateTimer");

		if (!isEndlessTime)
			StartCoroutine("RunTimer");

		score = 0;

		if (!Application.isEditor && Application.platform == RuntimePlatform.Android)
			MainMenu.SceneLoad();
	}

	private void Update()
	{
		if (!Validate_Force2.activeInHierarchy || Validate_Force.GetComponent<RectTransform>().localScale.x == 0)
			BarForce.GetComponent<Slider>().value = 0;

		Bar_indForce.transform.localScale = new Vector3(BarForce.GetComponent<Slider>().value, 1.0f, 1.0f);

		if (!isEndlessTime && timer <= 0)
		{
			Time_update = 20;
			Invoke(nameof(GameOver), 2);
		}

		if (Time_update <= 0 && !Application.isEditor)
			UpdatePoints();
	}

	public IEnumerator UpdateTimer()
	{
		while (true)
		{
			while ((Time_update > 0))
			{
				Time_update--;
				yield return new WaitForSeconds(1);
			}

			yield return new WaitForEndOfFrame();
		}
	}

	private IEnumerator RunTimer()
	{
		timer = _timerInitialValue;
		Time_update = Time_update_Max;
		while (timer > 0)
		{
			timer--;
			yield return new WaitForSeconds(1);
		}
	}

	public void Pause()
	{
		Time.timeScale = 0;
		Ball_img.gameObject.SetActive(false);
		PopUpExit.gameObject.SetActive(true);
		HUD.gameObject.SetActive(false);
	}

	public void CancelPause()
	{
		Time.timeScale = 1;
		Ball_img.gameObject.SetActive(true);
		PopUpExit.gameObject.SetActive(false);
		HUD.gameObject.SetActive(true);
	}

	public void Exit()
	{
		//Clear Unused Textures
		Resources.UnloadUnusedAssets();

		Time.timeScale = 1;
		Time.fixedDeltaTime = 0.02F * Time.timeScale;
		PlayerPrefs.SetInt("game_mode", -1);
		SceneManager.LoadScene("1. Menu");
	}

	public void GameOver()
	{
		Time.timeScale = 1;
		HUD.gameObject.SetActive(false);
		Ball_img.gameObject.SetActive(false);

		if (!isEndlessTime)
		{
			isEndlessTime = true;
			MainMenu.Exit();
			MainMenu.TotalPoints(score.ToString());
		}

		OnGameOver?.Invoke();
	}

	public void TryAgain()
	{
		Time.timeScale = 1;
		PlayerPrefs.SetString("nextLevel", "Beer pong");

		//Clear Unused Textures
		Resources.UnloadUnusedAssets();

		SceneManager.LoadScene("Beer_pong");
	}

	public void UpdatePoints()
	{
		MainMenu.UpdatePoints(score.ToString());
		Time_update = int.Parse(PlayerPrefs.GetString("updateTime"));
	}

	public void AddScore(int incrementAmount)
	{
		score += incrementAmount;
		OnScoreChanged?.Invoke(score);
	}
}