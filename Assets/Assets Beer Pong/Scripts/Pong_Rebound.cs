﻿using UnityEngine;

public class Pong_Rebound : MonoBehaviour
{
	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Beer_pong"))
			other.gameObject.tag = "Doble_Pong";
	}
}