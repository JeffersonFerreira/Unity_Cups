﻿using System;
using UnityEngine;

namespace BeerPong
{
	public class LauncherAimController : MonoBehaviour
	{
		[SerializeField] private MinMax _rotationLimit = new MinMax(-400, 400);
		[SerializeField] private float _dragSpeed = 60;

		[Space]
		[SerializeField] private float _verticalLaunchAngle = -20;
		[SerializeField] private float _horizontalAngleMultiplier = 1;

		[Space]
		[SerializeField] private RectTransform _gameplayAreaRectTransform;

		private Camera _camera;
		private Vector3 _dragOrigin;

		private void Awake()
		{
			_camera = Camera.main;
		}

		private void Update()
		{
			Vector3 mousePos = Input.mousePosition;

			// Ignore further input if player is touching on top of the screen   
			if (!RectTransformUtility.RectangleContainsScreenPoint(_gameplayAreaRectTransform, mousePos, _camera))
				return;

			if (Input.GetMouseButtonDown(0))
			{
				_dragOrigin = mousePos;
			}
			else if (Input.GetMouseButton(0))
			{
				float horizontalLaunchAngle = _dragSpeed * _camera.ScreenToViewportPoint(mousePos - _dragOrigin).x;

				float clamp = Mathf.Clamp(horizontalLaunchAngle, _rotationLimit.Min, _rotationLimit.Max);
				transform.localEulerAngles = new Vector3(_verticalLaunchAngle, clamp * _horizontalAngleMultiplier);
			}
		}
	}

	[Serializable]
	public struct MinMax
	{
		public float Min, Max;

		public MinMax(float min, float max)
		{
			this.Min = min;
			this.Max = max;
		}
	}
}