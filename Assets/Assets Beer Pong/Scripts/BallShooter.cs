﻿using System;
using UnityEngine;
using UnityEngine.UI;

public delegate void OnShoot(GameObject ball, GameObject ballShadow);

public class BallShooter : MonoBehaviour
{
	[SerializeField] private GameObject _inputColliderDetector;
	[SerializeField] private Slider _shootForceSlider;

	[Header("Prefab")]
	[SerializeField] private GameObject _ballPrefab;
	[SerializeField] private GameObject _ballShadowPrefab;

	[Header("Spawn points")]
	[SerializeField] private Transform _ballSpawnPoint;
	[SerializeField] private Transform _shadowSpawnPoint;

	public event OnShoot OnShoot;
	public event Action OnShootCooldown;

	private Camera _camera;

	private bool _fireOnUp;
	private bool _canShoot = true;

	private void Awake()
	{
		_camera = Camera.main;
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
			OnInputDown(Input.mousePosition);
		else if (Input.GetMouseButtonUp(0))
			OnInputUp();

		if (Input.touchCount > 0)
		{
			Touch touch = Input.GetTouch(0);

			if (touch.phase == TouchPhase.Began)
				OnInputDown(touch.position);
			else if (touch.phase == TouchPhase.Ended)
				OnInputUp();
		}

		// This was on GameManager
		// may be for testing purpose only???
		if (Input.GetButtonDown("Jump"))
			Shoot();
	}

	private void OnInputUp()
	{
		if (_fireOnUp)
		{
			_fireOnUp = false;
			Shoot();
		}
	}

	private void OnInputDown(Vector2 screenPoint)
	{
		Ray ray = _camera.ScreenPointToRay(screenPoint);
		RaycastHit2D hit2D = Physics2D.Raycast(ray.origin, ray.direction);

		if (hit2D.collider && hit2D.collider.gameObject == _inputColliderDetector)
			_fireOnUp = true;
	}

	private void RestoreCanShoot()
	{
		_canShoot = true;
		OnShootCooldown?.Invoke();
	}

	public void Shoot()
	{
		if (!_canShoot)
			return;

		_canShoot = false;
		Invoke(nameof(RestoreCanShoot), 1);

		float shootForce = _shootForceSlider.value;

		GameObject ball = Instantiate(_ballPrefab, _ballSpawnPoint.position, _ballSpawnPoint.rotation);
		GameObject ballShadow = Instantiate(_ballShadowPrefab, _shadowSpawnPoint.position, _shadowSpawnPoint.rotation);

		Destroy(ball, 20);
		Destroy(ballShadow, 20);

		if (ball.TryGetComponent(out Rigidbody rigidbody))
		{
			rigidbody.rotation = ball.transform.rotation;
			rigidbody.AddRelativeForce(Vector3.forward * shootForce, ForceMode.VelocityChange);
		}

		OnShoot?.Invoke(ball, ballShadow);
	}
}