

Notes on a few of the Soccer_SFX sounds:


The UI_Timer_** sounds are all 1 second in length, and were designed to work as loops. However, they also work if triggered as one-shot sounds.


UI_Selecting_Loop is designed to play (as a looping sound) while certain options are available. This loop should then stop when
UI_Selected is triggered (i.e. upon selection by the user).


Keeper_Save_1H_** sounds are one-handed saves, and
Keeper_Save_2H_** sounds are two-handed.