﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadMenu : MonoBehaviour
{
	private void Start()
	{
		//Clear Unused Textures
		Resources.UnloadUnusedAssets();
		SceneManager.LoadScene("1. Menu");
	}
}